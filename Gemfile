source 'https://rubygems.org'

gem 'rails', '~> 4.2.5.1'

# Use SCSS for stylesheets
gem 'sass-rails', '~> 4.0.3'
# Use Uglifier as compressor for JavaScript assets
gem 'uglifier', '>= 1.3.0'
# Use CoffeeScript for .js.coffee assets and views
gem 'coffee-rails', '~> 4.0.0'
# See https://github.com/rails/execjs#readme for more supported runtimes

# Use jquery as the JavaScript library
gem 'jquery-rails'
# Turbolinks makes following links in your web application faster. Read more: https://github.com/rails/turbolinks
#gem 'turbolinks'
# Build JSON APIs with ease. Read more: https://github.com/rails/jbuilder
gem 'jbuilder', '~> 2.0'
# bundle exec rake doc:rails generates the API under doc/api.
gem 'sdoc', '~> 0.4.0',          group: :doc

gem 'grape'
# Swagger
gem 'rack-cors', :require => 'rack/cors'
gem 'grape-swagger'
gem 'grape-swagger-rails'
gem 'grape-entity', '~> 0.5.1'
gem 'httparty'
gem "net-ldap", '0.14.0'

gem 'bio', '1.5.1'
gem 'bio-protparam'
gem 'builder'
gem 'bsearch'

# Use ActiveModel has_secure_password
# gem 'bcrypt', '~> 3.1.7'

gem 'rspec-rails', '3.3.0', :group => [:development, :test]
gem 'http_logger'
# Use unicorn as the app server
# gem 'unicorn'

# Use Capistrano for deployment
# gem 'capistrano-rails', group: :development

# Use debugger
# gem 'debugger', group: [:development, :test]
gem 'grape-kaminari'
gem 'figaro'
# Windows does not include zoneinfo files, so bundle the tzinfo-data gem
gem 'tzinfo-data', platforms: [:mingw, :mswin, :x64_mingw]
gem 'pry-byebug', :group => :development
gem 'pry-rails', :group => :development
gem 'pry-stack_explorer', :group => :development
gem 'web-console', '~> 2.0', :group => :development



group :test do
  gem "json-schema"
  gem "timecop"
  # The database_cleaner gem is defined as follows as instructed by the developers
  # of this gem, documented here: https://github.com/DatabaseCleaner/database_cleaner/issues/299#issuecomment-137705128
  gem 'database_cleaner', '1.5.3'
  gem "factory_girl_rails"
  gem "simplecov"
  gem 'simplecov-rcov'
  gem 'vcr', '~> 3.0', '>= 3.0.1'
  gem 'webmock', '~> 2.1'
end
